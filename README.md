# Gamle-Eksamener

Her ligger masse gamle eksamener. De som er lagt til nå, er hovedsakelig fra ITGK. ITGK har ikke samme pensum, og dermed blir mange av oppgavene utenfor vårt pensum. Men mye av det er innenfor.

Jeg fant forsåvidt denne nettsiden, som virker som den har en del bra øvingsmateriale på. Bruk den gjerne. Og scroll litt ned, og trykk deg inn på relevante "Outputs"-oppgaver.

https://www.geeksforgeeks.org/python-multiple-choice-questions/
