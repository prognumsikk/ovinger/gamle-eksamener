# TDT4109 tidligere eksamener

En oversikt over alle eksamene finnes [her](tidligere_eksamener.ipynb).

Skulle du finne noen feil, så send gjerne en mail til *tdt4109-undass@idi.ntnu.no*.
