'''
Her skal du programmere!


Backstory

Vi er så flinke til å gjøre økonomi, at vi hjelper flere andre selskap også.
Vi har en stor tabell, hvor kolonner er utgiftsposter, og rader er selskap vi
hjelper.
Vi går ut i fra at vi ikke vet hvor mange kolonner eller rader det er.
Den kan være 3x3, eller 2x4, eller hva som helst annet.
Koden må derfor være generell nok til å brukes på en tabell med vilkårlig
dimensjon.



utgifter = [
    # lønn, utstyr, annet
    [120, 1007, 320], # selskap 1
    [290, 500, 300],  # selskap 2
    [ 50, 500, 70]    # selskap 3
]



Kommentarer er lagt til for å vise hva de ulike radene/kolonnene representerer. 
Verdiene er representerer hvor mange tusenlapper hver post har. 
Altså bruker selskap 1 kr 120000 på lønn.



For å skaffe bedre oversikt over økonomien er det behov for vite totalverdien av de ulike postene.
Det betyr summen av all lønn, summen av alt utstyr, og summen av 'annet'.

Oppgave:

Lag funksjonen finn_summer(utgifter) som tar i mot en tabell av formatet beskrevet over, 
og returnerer en liste. Denne listen skal inneholde summen av hver enkelt 'kolonne: 
[sum_av_lønn, sum_av_utstyr, sum_av_annet, ...] Husk at du ikke vet hvor mange kolonner
vi har.

Kommenter eventuelle antagelser du tar.

Skriv ditt svar her

'''

utgifter = [
    # lønn, utstyr, annet
    [120, 1007, 320], # selskap 1
    [290, 500, 300],  # selskap 2
    [ 50, 500, 70]    # selskap 3
]

def finn_summer(utgifter):
    returliste = []
    # Listen på initialiseres. Enten kan vi lage like mange elementer med 0 som det er 
    # kolonner per rad. Alternativ kan vi bare lese inn første rad, og appende verdiene.
    # Deretter kunne vi gått igjennom på samme måte som første måte, men fjernet første rad.
    # Her er et eksempel på den første måten:   

    for i in range(len(utgifter[0])):
        returliste.append(0)

    for rad in utgifter:
        for k, verdi in enumerate(rad):
            returliste[k] += verdi
    
    return returliste

print(finn_summer(utgifter))
