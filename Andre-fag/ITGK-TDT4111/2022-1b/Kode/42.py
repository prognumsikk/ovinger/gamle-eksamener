'''
Funksjonen sjekk_medlemskap skal brukes for å filtrere elementer som er med i to grupper, etter følgende krav:
Gitt to lister 'kunder' og 'medlemmer' skal den returnere en ny liste.
Gjennomgangen er fra kundelisten sitt perspektiv.
Hvert element i denne nye listen gjenspeiler om kunden også finnes i medlemmer.
Hvis det finnes skal True legges til i listen, ellers skal en legge til False.
Eksempel: Hvis det første og andre elementet i listen kunder også finnes i listen medlem, men ikke det tredje elementet, vil dermed den returnerte listen starte slik:
[True, True, False, ...

Eksempler på kjøring:

>>> kunder = ['Paul', 'Børge', 'Guttorm']
>>> medlemmer = ['Børge', 'Dag Olav', 'Guttorm']
>>> sjekk_medlemskap(kunder, medlemmer)
[False, True, True]

>>> kunder = ['A', 'B', 'C', 'D']
>>> medlemmer = ['A', 'D', 'G', 'R', 'P', 'Å']
>>> sjekk_medlemskap(kunder, medlemmer)
[True, False, False, True]

Dra elementer inn på riktig sted for å oppfylle kravene gitt over:
'''

def sjekk_medlemskap(kunder, medlemmer):
    medlemskap_liste = []  # medlemskap_liste
    for kunde in kunder:  # kunde in kunder
        if kunde in medlemmer:  # kunde in medlemmer
            medlemskap_liste.append(True)  # True
        else:
            medlemskap_liste.append(False)  # False
    return medlemskap_liste

kunder = [x for x in range(20)]
medlemmer = [x for x in range(0,10,2)]

print(kunder)
print(medlemmer)
print(sjekk_medlemskap(kunder, medlemmer))
