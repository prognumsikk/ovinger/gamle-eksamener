# For å gjøre dette korrekt, må vi oversette alle 'aa' med 'å'


def tell_vokal_ord(filnavn):
    with open(filnavn) as f:
        text = f.read()
    text = text.lower()
    text = text.replace('aa', 'å')
    ordliste = text.split()

    antall_vokal_ord = 0
    for ord in ordliste:
        if ord[0] in 'aeiouyæøå':
            antall_vokal_ord += 1

    prosent = antall_vokal_ord / len(ordliste) * 100
    return prosent
