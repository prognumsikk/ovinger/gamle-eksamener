utgifter = [
    # lønn, utstyr, annet
    [120, 1007, 320], # selskap 1
    [290, 500, 300],  # selskap 2
    [ 50, 500, 70]    # selskap 3
]


def finn_summer(utgifter):
    returliste = [0] * len(utgifter[0])

    for rad in utgifter:
        for kol_index in range(len(rad)):
            returliste[kol_index] += rad[kol_index]
    
    return returliste


print(finn_summer(utgifter))
