'''
Funksjonen "trekk_fra_utgift" skal fjerne/trekke fra et antall kroner fra en
utgiftspost i dict-en "regnskap". Hvis regnskapet ikke har denne posten, skal
regnskapet ikke endres. Hvis denne posten ender opp med en verdi lik eller under 0,
skal denne posten fjernes. Funksjonen skal returnere den oppdaterte dict-en.

Eksempler på kjøring:

>>> trekk_fra_utgift({'lønn': 250, 'skatt': 100}, 'lønn', 100)
{'lønn': 150, 'skatt': 100}
# 'lønn'-posten er redusert med 100

>>> trekk_fra_utgift({'lønn': 100, 'skatt': 100}, 'lønn', 150)
{'skatt': 100}
# 'lønn'-posten er fjernet

>>> trekk_fra_utgift({'lønn': 100, 'skatt': 100}, 'lønn', 100)
{'skatt': 100}
'''

def trekk_fra_utgift(regnskap, utgiftspost, kroner):
    if utgiftspost not in regnskap:  # utgiftspost not in regnskap
        return regnskap

    regnskap[utgiftspost] -= kroner  # "= kroner" "-= utgiftspost[kroner]"
    
    if regnskap[utgiftspost] > 0:  # >= == (kun operator)
        return regnskap 
    del regnskap[utgiftspost]  # regnskap[utgiftspost]
    return regnskap # brenn denne


print(trekk_fra_utgift({'lønn': 250, 'skatt': 100}, 'lønn', 100))
print(trekk_fra_utgift({'lønn': 100, 'skatt': 100}, 'lønn', 150))
print(trekk_fra_utgift({'lønn': 100, 'skatt': 100}, 'lønn', 100))
