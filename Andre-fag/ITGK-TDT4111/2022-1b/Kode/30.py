def f(r, y):
    for i in range(len(y)):
        r[y[i]] = i + 1
    return r


print(sum(f({}, 'abcdef').values()))
print(sum(f({}, 'abba').values()))
