def finn_summer(utgifter):
    antall_kolonner = len(utgifter[0])
    summer = [0] * antall_kolonner
    for rad in utgifter:
        for kolonneindeks in range(len(rad)):
            summer[kolonneindeks] += rad[kolonneindeks]
    return summer



utgifter = [
    [120,     1007,     320],   # avdeling 1
    [290,      500,     300],   # avdeling 2
    [ 50,      500,      70]   # avdeling 3
]

print(finn_summer(utgifter))
