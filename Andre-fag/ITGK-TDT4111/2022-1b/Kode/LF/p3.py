def snitt_alder(members, name):
    # Finn yrkestittel or lag liste over aldere
    søk_yrke = members[name][2]
    aldere = []
    
    for member in members.values():
        alder, land, yrke = member
        if yrke == søk_yrke:
            aldere.append(alder)
    
    # Ta snitt av aldere
    snitt = sum(aldere) / len(aldere)
    return round(snitt)


members = {
    'Anne': [35, 'Norge', 'CEO'],
    'Bjarne': [55, 'Norge', 'sekretær'],
    'Carl': [45, 'USA', 'CEO'],
    'Diana': [28, 'UK', 'vaktmester']
}

snitt_alder(members, 'Anne')
