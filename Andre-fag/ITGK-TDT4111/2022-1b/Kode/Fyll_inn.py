

# Fyll inn 1
'''
Poenget med denne oppgaven er å se om de kan basis bruk
- max
- len
- endring av verdi på plass 1  endrer på det andre feltet
- remove spesifiserer ikke plassnummer, men verdi
- sum er jo enkel nok

- Er remove brukt, eller står i fuskelapp?
'''

# Følgende kode kjøres. Hva skrives ut?

liste = [4, 3, 2, 1, 0]
liste[1] = max(liste) + len(liste)
liste.remove(0)

print(sum(liste))



# Fyll inn 2_1
'''
Det jeg lurer litt på her er om fonten som brukes på eksamen er vanskelig.
Det handler egentlig bare om å se at en kan subste de som står rett overfor hverandre. 
'''

# Følgende kode kjøres.

tekst = "x423^)£9"
_1 = 'raykestfgnetåld'
_2 = '94jx35+v6^£7@2)'

ny_tekst = ''
for tegn in tekst:
    ny_tekst += tegn
    if tegn in _2:
        ny_tekst = ny_tekst.replace(tegn, _1[_2.find(tegn)])

print(ny_tekst)
# Hva skrives ut? Svaret skal skrives uten noen fnutter av noe slag foran eller bak - kun teksten. Ikke mist poeng på å ikke lese dette.


# Fyll_inn 2_2


print(ny_tekst[:2] + 'r' + ny_tekst[2:3] + '_p/v'[0:0])
