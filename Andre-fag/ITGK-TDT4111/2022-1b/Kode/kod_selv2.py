'''
Du elsker dikt! I lang tid har du laget disse diktene i en stadig lenger fil kalt "dikt.txt"
Filen har en slik struktur:

F: Jan Erik Vold
T: Dråpe
dråpen
henger der
ikke
T: Ved Rundarne
F: Aa. O. Vinje
No seer eg atter slike Fjøll og Dalar,
som deim eg i min fyrste Ungdom saag,
og sama Vind den heite Panna svalar;
og Gullet ligg paa Snjo, som fyrr det laag.
Det er eit Barnemaal, som til meg talar,
og gjer’ meg tankefull, men endaa fjaag
Med Ungdomsminni er den Tala blandad:
Det strøymer paa meg, so eg knapt kan anda.
aasmund strever
...

En linje som starter med strengen "F:" inneholder forfatterens navn
En linje som starter med strengen "T:" inneholder diktets tittel
Selve diktet følger så helt frem til neste linje med 'T:'


Nå har du fått for deg at du ønsker å kjøre en analyse på diktene dine. 
Hvor mange av ordene begynner på en vokal (og andre bokstav er en konsonant)?

Her er det et par krav og avgrensninger:
- Du skal ikke telle ord i forfatterens navn eller diktets tittel
- Du skal kun gi en total oppsummering, du trenger altså ikke ha noen oppsummering 'per dikt'
- Tellingen din skal være uavhengig av om det er store eller små bokstaver.
- Enkelte dikt bruker gammel stavemåte 'aa' for tegnet 'å'. Disse tilfelle skal også telles som 'å'.
- Du trenger _ikke_ gjøre tilsvarende sjekker for andre bokstaver som 'ae' (æ) og 'oe' (ø).
- Det finnes ord som kun består av én vokal. Disse skal _ikke_ telles med. (F.eks. ordet 'å')
- Du kan benytte deg av denne: 'vokaler = 'aeiouyæøå'

Skriv funksjonen antall_vokal_konsonant(filnavn). Denne skal oppfylle kravene nevnt over, og 
returnere antallet ord som starter med en vokal og der andre bokstav er en konsonant.

Eksempel på kjøring (forutsetter at filen dikt.txt inneholder diktene nevnt over): 

>>> print(antall_vokal_konsonant('dikt.txt'))
15

>>> # Ordene er: ikke eg atter og eg Ungdom og og er og enda Ungdomsminni er eg anda.
>>> # Det er ingen eksempler på ord som starter med 'aa' eller 'Aa' her.

'''

vokaler = 'aeiouyæøå'

def antall_vokal_konsonant(filnavn):
    
    # Lese inn innholdet i filen til en liste med tekstlinjer
    with open(filnavn, encoding = "utf-8") as f:
        linjer = f.readlines()

    antall_vokal_konsonant_ord = 0

    for linje in linjer:
        # Vi ble bedt om å hoppe over linjene med forfatter og diktnavn:
        if linje[:2] in ['T:', 'F:']: 
            continue

        # Så må vi gjøre om eventuelle 'Aa' eller 'aa' til 'å':
        # Dette kan vi gjøre her, så slipper vi aa gjøre det senere.
        linje = linje.replace('aa', 'a')
        linje = linje.replace('Aa','å')

        # NÅ kan vi dele opp ord, og sjekke om første bokstav er vokal og andre ikke.
        # Merk at en må kjøre en lower(). Litt trekk dersom en mangler sjekk for både
        # store og små bokstaver.
        for ord in linje.split():
            print(ord)
            # Her må vi sjekke om første tegn er vokal, så om ordet er mer enn ett tegn,
            # og i så fall om det andre er en konsonant. Python tester fra venstre mot
            # høyre, så hvis len(ord) > 1 feiler så vil den aldri prøve å hente ut ord[1].
            # # Det siste hadde jo ellers gjort at den krasjet. 
            if ord[0].lower() in vokaler and len(ord) > 1 and ord[1] not in vokaler:
                print(ord) # Bare for å se at det virker 
                antall_vokal_konsonant_ord += 1
    return antall_vokal_konsonant_ord

'''
'''

print(antall_vokal_konsonant('dikt.txt'))
