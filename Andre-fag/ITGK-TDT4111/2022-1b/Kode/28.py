a = {1, 2, 3, 4, 5, 4, 2}
b = {'a', 'b', 'c', 0, 1, 3, 2}

c = a.intersection(b)
d = b.difference(c)

d.discard(0)
d.discard(1)
d.discard(2)

e = sorted(d)

print(e[-1])
