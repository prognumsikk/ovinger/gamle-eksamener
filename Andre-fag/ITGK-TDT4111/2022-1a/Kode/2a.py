import pickle


def saker_og_ting(x, fname):

    fil = open(fname, 'wb')

    pickle.dump(x, fil)


def noe_annet(fname):

    fil = open(fname, 'rb')

    return pickle.load(fil)


x = [
    [1, 2, 3],
    [2, 3, 4]
]

fname = 'testfil'

saker_og_ting(x, fname)
ananas = noe_annet(fname)
print(ananas)
