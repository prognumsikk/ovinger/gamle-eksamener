def gjett_årstall():
    alder = input('Hvor gammel blir du i år? ')
    alder = int(alder)
    kull = 2022 - alder
    kommentar = f'Du blir {alder} år gammei i år'
    kommentar += f', og er dermed født i {kull}.'
    return kommentar


print(gjett_årstall())
