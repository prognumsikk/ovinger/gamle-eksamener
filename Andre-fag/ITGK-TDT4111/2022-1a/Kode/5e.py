def get_countries(members):
    countries = {}
    for member_info in members.values():
        country = member_info[1]
        if country not in countries:
            countries[country] = 0
        countries[country] += 1
    return countries


# Test 1
members = {
    'Anne': [35, 'Norge', 'CEO'],
    'Bjarne': [55, 'Norge', 'sekretær'],
    'Carl': [45, 'USA', 'software-utvikler'],
    'Diana': [28, 'UK', 'vaktmester']
}

expected_countries = {
    'Norge': 2,
    'USA': 1,
    'UK': 1
}

countries = get_countries(members)
assert countries == expected_countries, f'{countries} =/= {expected_countries}'

# Test 2
countries_2 = get_countries({'Paul': [28, 'Norge', 'forsker'], 'Obama': [55, 'USA', 'konge']})
expected_countries_2 = {'Norge': 1, 'USA': 1}
assert countries_2 == expected_countries_2, f'{countries_2} =/= {expected_countries_2}'

print('Yay! Worked! :D')
