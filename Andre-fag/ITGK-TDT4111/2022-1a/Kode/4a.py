def fiks_utgift(tall):
    if tall < 0:
        return -tall
    else:
        return tall


print(fiks_utgift(10_000))
print(fiks_utgift(-10_000))
