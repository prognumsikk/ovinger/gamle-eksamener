def find_matches(X, Y):
    X = set(X)
    for y in Y:
        if y not in X:
            return False
    return True


print(find_matches([1, 2, 3, 4, 5, 6, 7, 8, 9], [2, 1, 4, 5]))
print(find_matches([1, 2, 3, 4, 5, 6, 7, 8, 9], [2, 1, 4, 5, 12]))
