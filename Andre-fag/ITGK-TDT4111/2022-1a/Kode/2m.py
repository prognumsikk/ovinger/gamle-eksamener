a = False

b = True

c = a or b

d = b and not (a or b)

if c and d:

    print(1)

elif c and not d:

    print(2)

elif not c and d:

    print(3)

else:

    print(4)
