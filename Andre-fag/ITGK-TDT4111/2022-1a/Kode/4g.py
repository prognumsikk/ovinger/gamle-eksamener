def finn_rabatt(alder):
    if alder > 67:
        rabatt = 40
    elif alder <= 16:
        rabatt = 50
    elif alder <= 26:
        rabatt = 20
    else:
        rabatt = 0
    return rabatt


print(finn_rabatt(10))
print(finn_rabatt(20))
print(finn_rabatt(30))
print(finn_rabatt(90))
