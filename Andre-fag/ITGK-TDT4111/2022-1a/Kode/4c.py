def legg_til_utgift(regnskap, utgiftspost, kroner):
    utgiftspost = utgiftspost.lower()
    if utgiftspost not in regnskap:
        regnskap[utgiftspost] = 0
    regnskap[utgiftspost] += kroner
    return regnskap


regnskap = {}
print(regnskap)

regnskap = legg_til_utgift(regnskap, 'lønn', 50_000)
print(regnskap)

regnskap = legg_til_utgift(regnskap, 'Lønn', 20_000)
print(regnskap)
