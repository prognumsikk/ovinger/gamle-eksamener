# Gamle eksamener i TDT4111

## Naming
Mappenavnene/eksamennavnene er laget som følger `yyyy-XY`, hvor `yyyy` er årstallet kullet hadde faget (forelesningene), `X` er om det er ordinær eksamen (1) eller konteeksamen (2), og `Y` sier hvilken versjon det er. F.eks.

- `2022-1a`: Første ordinære eksamen (holdt desember 2022)
- `2022-1b`: Ekstraeksamen grunnet feil i ordinær eksamen (holdt januar 2023)
- `2022-2a`: Konteeksamen (holdt august (?) 2023)
- `2022-2b`: Muntlig konteeksamen (grunnet uværet Hans) (holdt september 2023)

Ergo er alle som starter med `2022` fra det kullet, men ikke nødvendigvis holdt det semesteret.
